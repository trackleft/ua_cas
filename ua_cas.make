api = 2
core = 7.x

; CAS contrib module.
projects[cas][version] = 1.5
projects[cas][subdir] = contrib
; Fix for stale cas_login_data entries (affects sites hosted in Acquia Cloud).
; @see https://jira.arizona.edu/browse/UADIGITAL-792
; @see https://www.drupal.org/node/2737897
projects[cas][patch][2737897] = http://drupal.org/files/issues/intermittent_fatal-2737897-11.patch

; phpCAS library.
libraries[CAS][download][type] = get
libraries[CAS][download][url] = https://github.com/Jasig/phpCAS/archive/1.3.4.tar.gz
